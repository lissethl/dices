import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from '@/components/admin/Dashboard';
import AppNotFound from '@/common/layout/pages/AppNotFound';
import AppForbidden from '@/common/layout/pages/AppForbidden';
import AppError from '@/common/layout/pages/AppError';

// System
import Login from '@/components/admin/auth/Login';
import Registro from '@/components/admin/auth/Registro';
import LoginCiudadania from '@/components/admin/auth/LoginCiudadania';
import LoginNit from '@/components/admin/auth/LoginNit';
import Account from '@/components/admin/account/Account';
import Entidad from '@/components/admin/entidad/Entidad';
import Usuario from '@/components/admin/usuario/Usuario';
import Modulo from '@/components/admin/modulo/Modulo';
import Preferencias from '@/components/admin/preferencias/Preferencias';
import Log from '@/components/admin/Log';
import Prueba from '@/components/admin/prueba/Prueba';
import Test from '@/components/admin/Test';
import Universidad from '@/components/parametros/universidad/Universidad';
import Carrera from '@/components/parametros/carrera/Carrera';
import TipoPrograma from '@/components/parametros/tipoprograma/TipoPrograma';
import GradoAcademico from '@/components/parametros/gradoacademico/GradoAcademico';
import Requisito from '@/components/parametros/requisito/Requisito';
import DatoGeneral from '@/components/parametros/datogeneral/DatoGeneral';
import OpcionPago from '@/components/parametros/opcionpago/OpcionPago';
import Horario from '@/components/parametros/horario/Horario';
import Tematica from '@/components/parametros/tematica/Tematica';
import DRequisito from '@/components/parametros/drequisitos/DRequisito';
import Programa from '@/components/gestionacademica/programa/Programa';
import Bloque from '@/components/gestionacademica/bloque/Bloque';
import ModulosPrograma from '@/components/gestionacademica/modulosprograma/ModulosPrograma';
import DetallesPrograma from '@/components/gestionacademica/detallesprograma/DetallesPrograma';
import Docente from '@/components/gestionacademica/docente/Docente';
import KardexDocente from '@/components/gestionacademica/docente/kardexDocente/KardexDocente';
import Preinscrito from '@/components/preinscripcion/preinscrito/Preinscrito';
import Inscrito from '@/components/preinscripcion/inscrito/Inscrito';
import Postulante from '@/components/preinscripcion/postulante/Postulante';
import Rezagado from '@/components/preinscripcion/postulante/Rezagado';
import PostulantePrograma from '@/components/preinscripcion/postulanteprograma/PostulantePrograma';
import AsignacionCorreo from '@/components/preinscripcion/asignacioncorreo/AsignacionCorreo';
import BajaPreinscrito from '@/components/preinscripcion/preinscritobajas/BajaPreinscrito';
import Entrevista from '@/components/preinscripcion/entrevista/Entrevista';
import Inscripcion from '@/components/preinscripcion/inscripcion/Inscripcion';
import Kardex from '@/components/preinscripcion/kardex/Kardex';
import AsignacionDocente from '@/components/gestionacademica/asignaciondocente/AsignacionDocente';
import Asignacion from '@/components/gestionacademica/asignaciondocente/Asignacion';
import ProgramaRequisito from '@/components/gestionacademica/programa/ProgramaRequisito';
import AdminPrograma from '@/components/gestionacademica/adminprogramas/AdminPrograma';
import Record from '@/components/gestionacademica/record/Record';
import Nota from '@/components/gestionacademica/notas/Nota';
import AsigModulo from '@/components/gestionacademica/asignacionmodulo/AsigModulo';
import Lasistencia from '@/components/gestionacademica/lasistencia/Lasistencia';
import Contrato from '@/components/financiera/contrato/Contrato';
import Pago from '@/components/financiera/pago/Pago';
import Pagosia from '@/components/financiera/pagosia/Pagosia';
import Reserva from '@/components/financiera/reserva/Reserva';
import EstudianteRequisito from '@/components/preinscripcion/preinscrito/EstudianteRequisito';
import ListaPostulante from '@/components/resportes/lpostulante/ListaPostulante';
import Srequisitos from '@/components/resportes/srequisitos/Srequisitos';
import ConsolidadoEvaluacion from '@/components/resportes/ConsolidadoEvaluacion';
import EvaluacionAlumno from '@/components/resportes/EvaluacionAlumno';
import EvaluacionDocente from '@/components/resportes/EvaluacionDocente';
import HistorialAcademico from '@/components/resportes/HistorialAcademico';
import RecordAcademico from '@/components/resportes/RecordAcademico';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/registro',
      name: 'Registro',
      component: Registro
    },
    {
      path: '/login-ciudadania',
      name: 'LoginCiudadania',
      component: LoginCiudadania
    },
    {
      path: '/login-nit',
      name: 'LoginNit',
      component: LoginNit
    },
    {
      path: '/usuarios',
      name: 'Usuario',
      component: Usuario
    },
    {
      path: '/account',
      name: 'Account',
      component: Account
    },
    {
      path: '/prueba',
      name: 'Prueba',
      component: Prueba
    },
    {
      path: '/entidades',
      name: 'Entidad',
      component: Entidad
    },
    {
      path: '/parametros',
      name: 'Preferencias',
      component: Preferencias
    },
    {
      path: '/modulos',
      name: 'Módulo',
      component: Modulo
    },
    {
      path: '/logs',
      name: 'Logs',
      component: Log
    },
    {
      path: '/test',
      name: 'Test',
      component: Test
    },
    // Parametros
    {
      path: '/universidades',
      name: 'Universidad',
      component: Universidad
    },
    {
      path: '/carreras',
      name: 'Carrera',
      component: Carrera
    },
    {
      path: '/tipoprograma',
      name: 'TipoPrograma',
      component: TipoPrograma
    },
    {
      path: '/gradosacademicos',
      name: 'GradoAcademico',
      component: GradoAcademico
    },
    {
      path: '/requisitos',
      name: 'Requisito',
      component: Requisito
    },
    {
      path: '/datosgenerales',
      name: 'DatoGeneral',
      component: DatoGeneral
    },
    {
      path: '/opcionespago',
      name: 'OpcionPago',
      component: OpcionPago
    },
    {
      path: '/horarios',
      name: 'Horario',
      component: Horario
    },
    {
      path: '/tematicas',
      name: 'Tematica',
      component: Tematica
    },
    {
      path: '/drequisitos',
      name: 'DRequisito',
      component: DRequisito
    },
    {
      path: '/inscripcion',
      name: 'Inscripcion',
      component: Inscripcion
    },
    // gestión académica
    {
      path: '/programas',
      name: 'Programa',
      component: Programa
    },
    {
      path: '/bloques/:id',
      name: 'Bloque',
      component: Bloque
    },
    {
      path: '/moduloprograma/:id',
      name: 'Módulos Programa',
      component: ModulosPrograma
    },
    {
      path: '/detalleprograma/:id',
      name: 'Detalles Programa',
      component: DetallesPrograma
    },
    {
      path: '/docentes',
      name: 'Docente',
      component: Docente
    },
    {
      path: '/kardexsDocentes/:id',
      name: 'KardexDocente',
      component: KardexDocente
    },
    {
      path: '/preinscritos',
      name: 'Preinscrito',
      component: Preinscrito
    },
    {
      path: '/adminprogramas',
      name: 'AdminPrograma',
      component: AdminPrograma
    },
    {
      path: '/records',
      name: 'Record',
      component: Record
    },
    {
      path: '/notas',
      name: 'Nota',
      component: Nota
    },
    {
      path: '/asignacionmodulos',
      name: 'AsigModulo',
      component: AsigModulo
    },
    //  gestión estudiantil
    {
      path: '/postulantes',
      name: 'Postulante',
      component: Postulante
    },
    {
      path: '/rezagados',
      name: 'Rezagado',
      component: Rezagado
    },

    {
      path: '/postulantesprograma',
      name: 'PostulantePrograma',
      component: PostulantePrograma
    },
    {
      path: '/asignacioncorreo/',
      name: 'AsignacionCorreo',
      component: AsignacionCorreo
    },
    {
      path: '/preinscritobajas',
      name: 'BajaPreinscrito',
      component: BajaPreinscrito
    },
    {
      path: '/entrevistas',
      name: 'Entrevista',
      component: Entrevista
    },
    {
      path: '/kardexs/:id',
      name: 'Kardex',
      component: Kardex
    },
    {
      path: '/asignacionDocentes',
      name: 'AsignacionDocente',
      component: AsignacionDocente
    },
    {
      path: '/asignaciones',
      name: 'Asignacion',
      component: Asignacion
    },
    {
      path: '/contratos',
      name: 'Contrato',
      component: Contrato
    },
    {
      path: '/programaRequisitos/:id',
      name: 'ProgramaRequisito',
      component: ProgramaRequisito
    },
    {
      path: '/estudianteRequisitos/:id',
      name: 'EstudianteRequisito',
      component: EstudianteRequisito
    },
    {
      path: '/inscritos',
      name: 'Inscrito',
      component: Inscrito
    },
    {
      path: '/lasistencia',
      name: 'Lasistencia',
      component: Lasistencia
    },
    // Gestión financiera
    {
      path: '/reservas',
      name: 'Reserva',
      component: Reserva
    },
    {
      path: '/pagos/:id',
      name: 'Pago',
      component: Pago
    },
    {
      path: '/siapagos',
      name: 'Pagosia',
      component: Pagosia
    },
    // reportes y estadisticas
    {
      path: '/lpostulantes',
      name: 'ListaPostulante',
      component: ListaPostulante
    },
    {
      path: '/srequisitos',
      name: 'Srequisitos',
      component: Srequisitos
    },
    {
      path: '/consolidadoevaluacion',
      name: 'ConsolidadoEvaluacion',
      component: ConsolidadoEvaluacion
    },
    {
      path: '/evaluacionalumno',
      name: 'EvaluacionAlumno',
      component: EvaluacionAlumno
    },
    {
      path: '/evaluaciondocente',
      name: 'EvaluacionDocente',
      component: EvaluacionDocente
    },
    {
      path: '/historialacademico',
      name: 'HistorialAcademico',
      component: HistorialAcademico
    },
    {
      path: '/recordacademico',
      name: 'recordacademico',
      component: RecordAcademico
    },
    {
      path: '/404',
      component: AppNotFound
    },
    {
      path: '/403',
      component: AppForbidden
    },
    {
      path: '/500',
      component: AppError
    },
    {
      path: '*',
      component: AppNotFound
    }
  ]
});
