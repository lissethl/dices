export default {
  data () {
    return {
      queryEvaluaciones: `
        query (
          $id_bloque: ID
          $id_modulo: ID
          $id_version: ID
          $id_programa: ID
          $id_docente: ID
          $id_estudiante: ID
          $tipo: String
          $estado: String
        ){
          evaluaciones(
            id_bloque: $id_bloque
            id_modulo: $id_modulo
            id_version: $id_version
            id_programa: $id_programa
            id_docente: $id_docente
            id_estudiante: $id_estudiante
            tipo: $tipo
            estado: $estado
          ) {
            rows {
              id
              evaluacion
              estado
              tipo
            }
            count
          }
        }
      `
    };
  },
  methods: {
    getEstudiantes (idRol = 5, obj = 'estudiantes') {
      this.$service.graphql({
        query: `
        query getEstudiantes {
          usuarios (id_rol: ${idRol}) {
            rows {
              id
              persona {
                primer_apellido
                segundo_apellido
                nombres
                nombre_completo
                nro_documento
                movil
                lugar_exp
              }
              email
            }
            count
          }
        }
      `,
        variables: {
        }
      }).then(response => {
        if (response) {
          this[obj] = response.usuarios.rows || [];
        }
      });
    }
  }
};
