/* eslint no-useless-escape: 0 */

'use strict';

export default {
  methods: {
    getConstantes (grupo = '', variable = 'constantes') {
      this.$service.graphql({
        query: `
          query getConstantes {
            constantes(grupo:"${grupo}") {
              count
              rows {
                id
                grupo
                codigo
                nombre
              }
            }
          }
        `,
        variables: {}
      }).then(response => {
        if (response) {
          this[variable] = response.constantes.rows;
        }
      });
    },
    printConstante (value, variable = 'constantes') {
      if (this[variable] && Array.isArray(this[variable])) {
        for (let item of this[variable]) {
          if (value === item.codigo) {
            return item.nombre;
          }
        }
      }
      return value;
    },
    printDocente (value) {
      console.log('value: ', value);
      value = value + '';
      this.$service.graphql({
        query: `
          query getDocente ($id: ID!) {
            usuario (id: $id){
                id
                persona {
                  nombre_completo
                }
              }
            }
          `,
        variables: {
          id: value
        }
      }).then(response => {
        if (response) {
          this.nombreDocente = response.usuario.persona.nombre_completo + '';
          console.log('nombre completo:', response.usuario.persona.nombre_completo);
          console.log('id de docente:', response.usuario.id);
        }
      });
      return 'lisseth';
    },
    getProgramas (sampleArg = '', variable = 'programas') {
      this.$service.graphql({
        query: `
        query getProgramas {
          programas (version: true, estado: ${sampleArg}) {
            count
            rows {
              id
              nombre
              version {
                id
                version
                periodo
              }
            }
          }
        }
        `,
        variables: {}
      }).then(response => {
        if (response) {
          this[variable] = response.programas.rows;
        }
      });
    },
    getBloques (idPrograma = 0, variable = 'bloques') {
      this.$service.graphql({
        query: `
        query getBloques {
          bloques (order: "numero", id_programa: ${idPrograma}){
            count
            rows{
              id
              id_programa
              nombre
              numero
              }
            }
          }
        `,
        variables: {}
      }).then(response => {
        if (response) {
          this[variable] = response.bloques.rows;
        }
      });
    }
  }
};
