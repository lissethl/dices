/* eslint no-useless-escape: 0 */

'use strict';

export default {
  methods: {
    getConstantes (grupo = '', variable = 'constantes') {
      this.$service.graphql({
        query: `
          query getConstantes {
            constantes(grupo:"${grupo}") {
              count
              rows {
                id
                grupo
                codigo
                nombre
              }
            }
          }
        `,
        variables: {}
      }).then(response => {
        if (response) {
          this[variable] = response.constantes.rows;
        }
      });
    },
    printConstante (value, variable = 'constantes') {
      if (this[variable] && Array.isArray(this[variable])) {
        for (let item of this[variable]) {
          if (value === item.codigo) {
            return item.nombre;
          }
        }
      }
      return value;
    },
    getUniversidades (variable = 'universidades') {
      this.$service.graphql({
        query: `
          query getUniversidades {
            universidades {
              count
              rows {
                id
                nombre
              }
            }
          }
        `,
        variables: {}
      }).then(response => {
        if (response) {
          this[variable] = response.universidades.rows;
        }
      });
    },

    printUniversidad (value, variable = 'universidades') {
      if (this[variable] && Array.isArray(this[variable])) {
        for (let item of this[variable]) {
          if (value === item.id) {
            return item.nombre;
          }
        }
      }
      return value;
    },
    getProgramas (sampleArg = '', variable = 'programas') {
      this.$service.graphql({
        query: `
          query getProgramas {
            programas (version: true, estado:${sampleArg}) {
              count
              rows {
                id
                nombre
                version {
                  id
                  version
                  periodo
                }
              }
            }
          }
        `,
        variables: {}
      }).then(response => {
        if (response) {
          this[variable] = response.programas.rows;
          console.log('programas:', response.programas.rows);
        }
      });
    },
    getBloques (idPrograma = 0, variable = 'bloques') {
      this.$service.graphql({
        query: `
        query getBloques {
          bloques (id_programa: ${idPrograma}){
            count
            rows{
              id
              id_programa
              nombre
              }
            }
          }
        `,
        variables: {}
      }).then(response => {
        if (response) {
          this[variable] = response.bloques.rows;
        }
      });
    }
    /*  getVersion (variable = 'destallesprograma') {
      this.$service.graphql({
        query: `
          query getProgramas {
            programas {
              count
              rows {
                id
                nombre
              }
            }
          }
        `,
        variables: {}
      }).then(response => {
        if (response) {
          this[variable] = response.programas.rows;
        }
      });
    } */
  }
};
