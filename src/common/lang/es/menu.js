export default {
  entidades: 'Entidades',
  configuracion: 'Configuraciones',
  usuarios: 'Usuarios',
  modulos: 'Módulos y permisos',
  roles: 'Roles',
  parametros: 'Preferencias',
  logs: 'Logs del sistema',
  prueba: 'Prueba',
  config: 'Configuraciones'
};
