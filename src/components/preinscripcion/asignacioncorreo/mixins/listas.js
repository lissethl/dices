/* eslint no-useless-escape: 0 */

'use strict';

export default {
  methods: {
    getConstantes (grupo = '', variable = 'constantes') {
      this.$service.graphql({
        query: `
          query getConstantes {
            constantes(grupo:"${grupo}") {
              count
              rows {
                id
                grupo
                codigo
                nombre
              }
            }
          }
        `,
        variables: {}
      }).then(response => {
        if (response) {
          this[variable] = response.constantes.rows;
        }
      });
    },
    printConstante (value, variable = 'constantes') {
      if (this[variable] && Array.isArray(this[variable])) {
        for (let item of this[variable]) {
          if (value === item.codigo) {
            return item.nombre;
          }
        }
      }
      return value;
    },
    getProgramas (variable = 'programas') {
      this.$service.graphql({
        query: `
          query getProgramas {
            programas (version: true) {
              count
              rows {
                id
                nombre
                version {
                  id
                  version
                  periodo
                }
              }
            }
          }
        `,
        variables: {}
      }).then(response => {
        if (response) {
          this[variable] = response.programas.rows;
          console.log('programas:', response.programas.rows);
        }
      });
    }
  }
};
