'use strict';

import { getField, updateField } from 'vuex-map-fields';

const layout = {
  namespaced: true,
  state: {
    form: {
      id: '',
      usuario: '',
      contrasena: '',
      email: '',
      cargo: '',
      g_academico: '',
      lugar_exp: '',
      lugar_nacimiento: '',
      direccion: '',
      mencion: '',
      acronimo: '',
      tipo_docente: '',
      id_universidad: '',
      carrera: '',
      ug_academico: '',
      id_programa: '',
      id_version: '',
      ref_oferta: '',
      ref_estudios: '',
      idea_tesis: '',
      estado_estudiante: '',
      tipo_contacto: '',
      fecha_entrevista: '',
      hora_entrevista: '',
      e_institucional: '',
      c_institucional: '',
      lugar_entrevista: '',
      requisitos: '',
      estado: '',
      id_entidad: null,
      id_rol: null,
      id_persona: null,
      tipo_documento: '',
      tipo_documento_otro: '',
      nro_documento: '',
      fecha_nacimiento: '',
      nombres: '',
      primer_apellido: '',
      segundo_apellido: '',
      nombre_completo: '',
      telefono: '',
      movil: '',
      nacionalidad: '',
      pais_nacimiento: '',
      genero: '',
      estado_persona: '',
      persona: null
    }
  },
  getters: {
    getField
  },
  mutations: {
    updateField,
    cleanForm (state) {
      state.form = {
        id: '',
        usuario: '',
        contrasena: '',
        email: '',
        cargo: '',
        g_academico: '',
        lugar_exp: '',
        lugar_nacimiento: '',
        direccion: '',
        mencion: '',
        acronimo: '',
        tipo_docente: '',
        id_universidad: '',
        carrera: '',
        ug_academico: '',
        id_programa: '',
        id_version: '',
        ref_oferta: '',
        ref_estudios: '',
        idea_tesis: '',
        estado_estudiante: '',
        tipo_contacto: '',
        fecha_entrevista: '',
        hora_entrevista: '',
        lugar_entrevista: '',
        requisitos: '',
        e_institucional: '',
        c_institucional: '',
        estado: '',
        id_entidad: null,
        id_rol: null,
        id_persona: null,
        tipo_documento: '',
        tipo_documento_otro: '',
        nro_documento: '',
        fecha_nacimiento: '',
        nombres: '',
        primer_apellido: '',
        segundo_apellido: '',
        nombre_completo: '',
        telefono: '',
        movil: '',
        nacionalidad: '',
        pais_nacimiento: '',
        genero: '',
        estado_persona: '',
        persona: null
      };
    },
    setForm (state, data) {
      for (let key in data) {
        state.form[key] = data[key];
      }
    }
  }
};

export default layout;
