/* eslint no-useless-escape: 0 */

'use strict';

export default {
  methods: {
    getBloques (idPrograma = 0, variable = 'bloques') {
      this.$service.graphql({
        query: `
        query getBloques {
          bloques (id_programa: ${idPrograma}){
            count
            rows{
              id
              id_programa
              nombre
              }
            }
          }
        `,
        variables: {}
      }).then(response => {
        if (response) {
          this[variable] = response.bloques.rows;
        }
      });
    }
  }
};
